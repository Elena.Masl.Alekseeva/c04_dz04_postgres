﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c04_dz04_Pastgrees
{
    public static class SQLScripts
    {

        /// <summary>
        /// Строка для скрипта создания таблицы пользователей
        /// </summary>
        public const string sqlCreateTableHumanInfo = @"

CREATE SEQUENCE Human_ID_SEQ
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 999999999
    CACHE 1
    ;

CREATE TABLE HumanInfoProject
(
    ID bigint NOT NULL DEFAULT NEXTVAL('Human_ID_SEQ'),
    FirstName character varying NOT NULL,
    LastName character varying NOT NULL,
    MidName character varying ,
    PhoneNumber character varying ,
    Email character varying ,
    BirthDate date,
    IsDeleted boolean,
    DeleteDate date,
    CONSTRAINT HumanInformation_pkey PRIMARY KEY(ID)
)

TABLESPACE pg_default;
";
        /// <summary>
        /// Строка для скрипта создания таблицы объявлений
        /// </summary>
        public const string sqlCreateTableAnnouncement = @"

CREATE SEQUENCE Announce_ID_SEQ
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 999999999
    CACHE 1
    ;

CREATE TABLE Announcement
(
    ID bigint NOT NULL DEFAULT NEXTVAL('Announce_ID_SEQ'),
    ID_Human bigint NOT NULL, 
    DateOfCreated date NOT NULL,
    Description character varying NOT NULL,
    Price character varying NOT NULL,
    IsActiv boolean,
    CONSTRAINT Announce_pkey PRIMARY KEY(ID),
    CONSTRAINT Communic_fkey_ID_Human FOREIGN KEY(ID_Human) REFERENCES HumanInfoProject(ID)
)

TABLESPACE pg_default;
";

        /// <summary>
        /// Строка для скрипта создания таблицы объявлений
        /// </summary>
        public const string sqlAnnouncementFiles = @"

CREATE SEQUENCE AnnounceFiles_ID_SEQ
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 999999999
    CACHE 1
    ;

CREATE TABLE AnnouncementFiles
(
    ID bigint NOT NULL DEFAULT NEXTVAL('AnnounceFiles_ID_SEQ'),
    ID_Announce bigint NOT NULL, 
    DateOfCreated date NOT NULL,
    PathStorage character varying NOT NULL,
    IsActiv boolean,
    CONSTRAINT AnnouncementFiles_pkey PRIMARY KEY(ID),
    CONSTRAINT AnnFiles_fkey_ID_Announce FOREIGN KEY(ID_Announce) REFERENCES Announcement(ID)
)

TABLESPACE pg_default;
";
        /// <summary>
        /// Строка для скрипта создания таблицы объявлений
        /// </summary>
        public const string sqlInsertHuman = @"

INSERT INTO HumanInfoProject (    
    FirstName,
    LastName,
    MidName,
    PhoneNumber,
    Email,
    BirthDate,
    IsDeleted,
    DeleteDate) 
VALUES(    
    :FirstName,
    :LastName,
    :MidName,
    :PhoneNumber,
    :Email,
    :BirthDate,
    :IsDeleted,
    :DeleteDate) RETURNING id; 

";
        /// <summary>
        /// Строка для скрипта создания таблицы объявлений
        /// </summary>
        public const string sqlInsertAnnounce = @"

INSERT INTO Announcement (    
    ID_Human, 
    DateOfCreated,
    Description,
    Price,
    IsActiv
) 
VALUES
(    
    :ID_Human, 
    :DateOfCreated,
    :Description,
    :Price,
    :IsActiv) RETURNING id; 
";







        /// <summary>
        /// Строка для скрипта создания таблицы файлов
        /// </summary>
        public const string sqlInsertFiles = @"

INSERT INTO AnnouncementFiles (    
    ID_Announce, 
    DateOfCreated,
    PathStorage,
    IsActiv
) 
VALUES
(    
    :ID_Announce, 
    :DateOfCreated,
    :PathStorage,
    :IsActiv) RETURNING id; 
";
        /// <summary>
        /// Строка для скрипта выбрать таблицу 
        /// </summary>
        public const string sqlSelectHumans = @"

SELECT * FROM public.humaninfoproject
ORDER BY id ASC 
";
    }
}
