﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBClassLbrary
{
    /// <summary>
    /// Класс объявления
    /// </summary>
    public class Announcement : IDataBD, IAnnouncement
    {
        private Int32 _ID;
        private DateTime _DateOfCreated;
        private string _Description;
        private double _Price;
        private bool _isActiv;


        public Int32 ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime DateOfCreated 
        {
            get
            {
                return _DateOfCreated;
            }
            set
            {
                _DateOfCreated = value;
            }
        }

        /// <summary>
        /// Описание 
        /// </summary>
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }
        }

        /// <summary>
        /// Цена
        /// </summary>
        public double Price 
        {
            get
            {
                return _Price;
            }
            set
            {
                _Price = value;
            } 
        }

        /// <summary>
        /// Является ли активным
        /// </summary>
        public bool IsActiv
        {
            get
            {
                return _isActiv;
            }
            set
            {
                _isActiv = value;
            }
        }
    }

    /// <summary>
    /// Класс пользователя
    /// </summary>
    public class HumanInformation : IDataBD, IHumanInformation
    {
        private Int32 _ID;
        private string _FirstName;
        private string _LastName;
        private string _MidName;
        private string _PhoneNumber;
        private string _Email;
        private bool _IsDeleted = false;
        private string _DeleteDate;
        private DateTime _BirthDate;

        public Int32 ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }
        /// <summary>
        /// Флаг: был ли пациент удален
        /// </summary>
        public bool IsDeleted
        {
            get { return _IsDeleted; }
            set
            {
                _IsDeleted = value;
            }
        }
        /// <summary>
        /// Дата удаления
        /// </summary>
        public string DeleteDate
        {
            get { return _DeleteDate; }
            set
            {
                _DeleteDate = value;
            }
        }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime BirthDate
        {
            get { return _BirthDate; }
            set
            {
                _BirthDate = value;
            }
        }

        /// <summary>
        /// Метод для расчета возраста на момент исследований
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public TimeSpan CalcAge(DateTime date)
        {
            return (date - _BirthDate);
        }

        /// <summary>
        /// Возраст
        /// </summary>
        public TimeSpan Age
        {
            get { return CalcAge(DateTime.Now); }
        }

        /// <summary>
        /// адрес электронной почты
        /// </summary>
        public string Email
        {
            get { return _Email; }
            set
            {
                _Email = value;
            }
        }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName

        {
            get { return _FirstName; }
            set
            {
                _FirstName = value;
            }
        }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName

        {
            get { return _LastName; }
            set
            {
                _LastName = value;
            }
        }
        /// <summary>
        /// Отчество
        /// </summary>
        public string MidName

        {
            get { return _MidName; }
            set
            {
                _MidName = value;
            }
        }
        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber

        {
            get { return _PhoneNumber; }
            set
            {
                _PhoneNumber = value;
            }
        }

    }

    public class UserDBAnnouncements : IDataBD
    {
        private Int32 _ID;
        private Int32 _UsersId;
        private Int32 _AnnouncementsId;
         
        /// <summary>
        /// ID записи 
        /// </summary>
        public Int32 ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }

        /// <summary>
        /// ID Пользователя
        /// </summary>
        public Int32 UsersId
        {
            get
            {
                return _UsersId;
            }
            set
            {
                _UsersId = value;
            }
        }

        /// <summary>
        /// ID объявления
        /// </summary>
        public Int32 AnnouncementsId
        {
            get
            {
                return _AnnouncementsId;
            }
            set
            {
                _AnnouncementsId = value;
            }
        }

    }
}
