﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBClassLbrary
{
    /// <summary>
    /// Интерфейс объявления
    /// </summary>
    public interface IAnnouncement
    {/*
        private DateTime _DateOfCreated;
        private string _Description;
        private double _Price;
        private bool _isActiv;
       */ 

        /// <summary>
        /// Дата создания
        /// </summary>
        DateTime DateOfCreated { get; set; }

        /// <summary>
        /// Описание 
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        double Price { get; set; }

        /// <summary>
        /// Является ли активным
        /// </summary>
        bool IsActiv { get; set; }
    }
}
