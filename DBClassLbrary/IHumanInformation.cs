﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBClassLbrary
{
    public interface IHumanInformation
    {

        /// <summary>
        /// Флаг: был ли пациент удален
        /// </summary>
        bool IsDeleted { get; set; }

        /// <summary>
        /// Дата удаления
        /// </summary>
        string DeleteDate { get; set;}

        /// <summary>
        /// Дата рождения
        /// </summary>
        DateTime BirthDate { get; set; }

        /// <summary>
        /// Метод для расчета возраста на момент исследований
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        TimeSpan CalcAge(DateTime date);

        /// <summary>
        /// Возраст
        /// </summary>
        TimeSpan Age { get; }
       

        /// <summary>
        /// адрес электронной почты
        /// </summary>
        string Email { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        string MidName { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        string PhoneNumber { get; set; }

    }

}
