﻿using c04_dz04_Pastgrees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c04_dz04_Postgrees
{
    class Program
    {
        const string connectString = "Host=localhost;Port=5432;Username=postgres;Password=123;Database=postgres";

        /// <summary>
        /// Выполнение команды
        /// </summary>
        /// <param name="sqlCreateTableHumanInfo"></param>
        /// <param name="connectionInProj">Коннекшн</param>
        private static void CommandGo(string sqlCreateTable, Npgsql.NpgsqlConnection connectionInProj)
        {
            using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(cmdText: sqlCreateTable, connection: connectionInProj))
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Создание таблиц
        /// </summary>
        /// <param name="connectionInProj">Коннекшн</param>
        private static void CreateDataBases(Npgsql.NpgsqlConnection connectionInProj)
        {
            CommandGo(SQLScripts.sqlCreateTableHumanInfo, connectionInProj);
            CommandGo(SQLScripts.sqlCreateTableAnnouncement, connectionInProj);
            CommandGo(SQLScripts.sqlAnnouncementFiles, connectionInProj);
        }

        /// <summary>
        /// Запись в таблицу пользователей
        /// </summary>
        /// <param name="connectionInProj">Коннекшн</param>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="MidName"></param>
        /// <param name="PhoneNumber"></param>
        /// <param name="Email"></param>
        /// <returns></returns>
        private static Int64 InsertHumanRecord(Npgsql.NpgsqlConnection connectionInProj, string FirstName, string LastName, string MidName, string PhoneNumber, string Email)
        {
            Int64 idHumanAdded = -1;
            using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(cmdText: SQLScripts.sqlInsertHuman, connection: connectionInProj))
            {
                Npgsql.NpgsqlParameterCollection parameters = command.Parameters;
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "FirstName", value: FirstName));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "LastName", value: LastName));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "MidName", value: MidName));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "PhoneNumber", value: PhoneNumber));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "Email", value: Email));

                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "BirthDate", value: DateTime.Now));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "IsDeleted", value: Convert.ToBoolean(0)));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "DeleteDate", value: DateTime.MinValue));

                idHumanAdded = Convert.ToInt64(command.ExecuteScalar());
            }

            return idHumanAdded;
        }

        /// <summary>
        /// Запись в таблицу объявлений
        /// </summary>
        /// <param name="connectionInProj">Коннекшн</param>
        /// <param name="idHumanAdded"></param>
        /// <param name="Description"></param>
        /// <param name="Price"></param>
        /// <returns></returns>
        private static Int64 InsertAnnounceRecord(Npgsql.NpgsqlConnection connectionInProj, Int64 idHumanAdded, string Description, string Price)
        {
            Int64 idAnnAdded = -1;
            using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(cmdText: SQLScripts.sqlInsertAnnounce, connection: connectionInProj))
            {
                Npgsql.NpgsqlParameterCollection parameters = command.Parameters;
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "ID_Human", value: idHumanAdded));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "DateOfCreated", value: DateTime.Now));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "Description", value: Description));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "Price", value: Price));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "IsActiv", value: Convert.ToBoolean(1)));

                idAnnAdded = Convert.ToInt64(command.ExecuteScalar());
            }

            return idAnnAdded;
        }

        /// <summary>
        /// Запись в таблицу хранения путей прикрепляемых файлов
        /// </summary>
        /// <param name="connectionInProj">Коннекшн</param>
        /// <param name="idAnnAdded"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        private static Int64 InsertAnnounceFileRecord(Npgsql.NpgsqlConnection connectionInProj, Int64 idAnnAdded, string path)
        {
            Int64 idFilesAdded = -1;
            using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(cmdText: SQLScripts.sqlInsertFiles, connection: connectionInProj))
            {
                Npgsql.NpgsqlParameterCollection parameters = command.Parameters;
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "ID_Announce", value: idAnnAdded));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "DateOfCreated", value: DateTime.Now));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "PathStorage", value: path));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "IsActiv", value: Convert.ToBoolean(1)));

                idFilesAdded = Convert.ToInt64(command.ExecuteScalar());
            }

            return idFilesAdded;
        }




        /// <summary>
        /// Заполняем базы
        /// </summary>
        /// <param name="connectionInProj">Коннекшн</param>
        private static void FullDataBases(Npgsql.NpgsqlConnection connectionInProj)
        {
            for (int i = 0; i < 5; i++)
            {
                Int64 idHuman = InsertHumanRecord(connectionInProj, string.Format("User {0}", i), string.Empty, string.Empty, string.Format("{0}{0}{0}{0}", i), string.Format("{0}@mail.ru", i));

                for (int j = 0; j < 2; j++)
                {
                    Int64 idAnn = InsertAnnounceRecord(connectionInProj, idHuman, string.Format("Description {0}", j), string.Format("{0}{0}{0}{0}", j));

                    for (int k = 0; k < 3; k++)
                    {
                        Int64 idFile = InsertAnnounceFileRecord(connectionInProj, idAnn, String.Format(@"\FileStorage\{0}\{1}\{2}", idHuman, idAnn, k));
                    }
                }

            }
        }

        /// <summary>
        /// Вывод данных из таблицы
        /// </summary>
        /// <param name="connectionInProj">Коннекшн</param>
        /// <param name="tableName">Имя таблицы</param>
        private static void OutputTable(Npgsql.NpgsqlConnection connectionInProj, string tableName)
        {
            Console.WriteLine("{0} data : ", tableName);
            string sqlStr = string.Format("SELECT * FROM {0}", tableName);
            using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(sqlStr, connection: connectionInProj))
            {
                Npgsql.NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {

                    string res = string.Empty;
                    for (int i = 0; i < reader.FieldCount; i++)
                        res += "\t" + reader.GetName(i).ToString();

                    Console.WriteLine(res);
                    res = string.Empty;
                    while (reader.Read()) // построчно считываем данные
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string dp = (!string.IsNullOrEmpty(reader.GetValue(i).ToString()) ? reader.GetValue(i).ToString() : "-");
                            res += "\t" + dp;
                        }
                        Console.WriteLine(res);
                        res = string.Empty;
                    }
                }
                reader.Close();
            }
        }

        /// <summary>
        /// Вывод данных их таблиц
        /// </summary>
        /// <param name="connectionInProj"></param>
        private static void OutputData(Npgsql.NpgsqlConnection connectionInProj)
        {
            OutputTable(connectionInProj, "HumanInfoProject");
            OutputTable(connectionInProj, "Announcement");
            OutputTable(connectionInProj, "AnnouncementFiles");
        }

        static void Main(string[] args)
        {

            using (Npgsql.NpgsqlConnection connectionInProj = new Npgsql.NpgsqlConnection(connectString))
            {
                connectionInProj.Open();
                CreateDataBases(connectionInProj);
                FullDataBases(connectionInProj);
                OutputData(connectionInProj);

            }
                
            Console.ReadKey();

        }
    }
}
